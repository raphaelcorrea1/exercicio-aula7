const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

app.get('/clientes',(req,res)=>{
    let data = req.query;
    let retorno = "Nome:" + data.nome;
    res.send("{dados: "+retorno+" }");
});


app.post('/clientes',(req,res)=>{
    let data = req.body;
    let hdr_tk = req.headers['access'];
    console.log("access: " + hdr_tk);
    let retorno = "Nome: " + data.nome;
    if(hdr_tk == "123456") {
        res.send('{dado: ' + retorno+ ' }');
    } else {
        res.send('Token inválido! ');
    }
});

app.get('/funcionarios', (req,res)=>{
    let data = req.query;
    let retorno = "Nome: " + data.nome;
    res.send("{dados: "+ retorno+ " }")
});

app.delete('/funcionarios/:param', (req,res)=>{
    let data = req.params.param;
    let hdr_tk = req.headers['access'];
    if(hdr_tk == 123456) {
        let retorno = data;
        res.send("{dados via parametro: " + retorno + "}");
    } else {
        res.send("Token inválido!");
    }
});

app.put('/funcionarios', (req,res)=>{
    let dado = req.body;
    let retorno = "Nome: " + dado.nome;
    let hdr_tk = req.headers['access'];
    if(hdr_tk == 123456) {
        res.send("{dados: " + retorno + " }")
    } else {
        res.send("Token inválido! ");
    }
});